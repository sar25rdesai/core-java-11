package com.combino.modifiers;

public class ExampleAbstractClassImpl extends ExampleAbstractClass{

    @Override
    public void exampleMethod() {
        System.out.println("this is an implementation for the abstract method");
    }
}
