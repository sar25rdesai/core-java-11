package com.combino.modifiers;


/*
 * public -- visible everywhere in the project
 * private -- visible only with in the class
 * protected -- visible within the package and to the sub/ child classes as well
 * default -- visible only with in the package
 * */
public class ExampleModifiers {
    protected String name;
    int id;
    private String address;

    private void examplePrivateMethod(){
        // this method wll be visible inside the class
    }
}
