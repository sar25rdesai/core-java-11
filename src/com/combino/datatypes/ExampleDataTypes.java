package com.combino.datatypes;

public class ExampleDataTypes {

    private byte aByte = -128;
    private short aShort = -32768;
    private int anInt = -2147483648;
    private long aLong = -9223372036854775808L;
    private float aFloat = 2147483647F;
    private double aDouble = -9223372036854775808D;
    private boolean aBoolean = true;
    private char aChar ='8';
    private String aString = "Chennai";
    private String bString = "Tamilnadu";
    private String cString = "India";

    public void printDateTypes() {
        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);
        System.out.println(aFloat);
        System.out.println(aDouble);
        System.out.println(aBoolean);
        System.out.println(aChar);
        System.out.println(aString);
        System.out.println(bString);
        System.out.println(cString);
    }
}
