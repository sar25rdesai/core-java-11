package com.combino.decisionMaking;

public class Tester {
    public static void main(String[] args) {

        int x = 24;
        char grade = 'C';
        ExampleIfStatement ifStatement = new ExampleIfStatement();
        ifStatement.makeADecision(x);
        ExampleSwitchCase switchCase = new ExampleSwitchCase();
        switchCase.decideGrade(grade);
        switchCase.decideTheValueOfX();
    }
}
