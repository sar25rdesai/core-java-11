package com.combino.loops;

public class ExampleWhileLoop {
    private int i = 10;

    public void printWhile(){

        while (i<20){
            System.out.println(i);
            i++;
        }

    }

    public void doWhile(){

        do {
            System.out.println(i);
        }
        while (i<10);
    }

}
