package com.combino.loops;

public class ExampleForLoop {
    int[] ids = {1001,1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009};

    public void printForLoop() {
        for (int i =0; i < ids.length; i++) {
            System.out.println(ids[i]);
        }
        System.out.println("***********************************************");

        for (int id:ids) {
            System.out.println(id);
        }
    }
}
