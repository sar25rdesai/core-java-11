package com.combino.loops;

public class Tester {
    public static void main(String[] args) {
        ExampleForLoop forLoop = new ExampleForLoop();
        forLoop.printForLoop();
        System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&");
        ExampleWhileLoop whileLoop = new ExampleWhileLoop();
        whileLoop.printWhile();
        System.out.println("***********do while *************");
        whileLoop.doWhile();
    }
}
