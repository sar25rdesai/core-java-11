package com.combino.strings;

public class Tester {
    public static void main(String[] args) {
        ExampleString exampleString = new ExampleString();
        exampleString.printString();
        exampleString.concatinatingStrings();

        ExampleStringBufferAndBuilder bufferAndBuilder = new ExampleStringBufferAndBuilder();
        bufferAndBuilder.printStringBuilderAndBuffer();
    }
}
