package com.combino.date;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExampleDate {

    public void printDateMethods() throws InterruptedException {
        Date date =new Date();
        System.out.println(date);

        System.out.println("**** now the program will wait for 10 seconds");
        Thread.sleep(20000);
        Date later = new Date();
        System.out.println(later);

        System.out.println(date.getTime());
        System.out.println(date.before(later));
        System.out.println(later.after(date));
        System.out.println(later.equals(date));

        System.out.println(later.compareTo(date));
        System.out.println(date.hashCode());

        System.out.println(date.toString());


    }


    public void printDateDFormats(){
        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
        System.out.println(ft.format(date));
    }
}
