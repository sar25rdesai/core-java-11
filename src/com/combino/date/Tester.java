package com.combino.date;

public class Tester {
    public static void main(String[] args) throws InterruptedException {
        ExampleDate date = new ExampleDate();
        date.printDateMethods();
        date.printDateDFormats();
        System.out.println("*****************Gregorian calender***********");
        ExampleGregorianCalender gregorianCalender = new ExampleGregorianCalender();
        gregorianCalender.printGregorianCalender();

    }

}
