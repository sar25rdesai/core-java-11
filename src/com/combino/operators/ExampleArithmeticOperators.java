package com.combino.operators;

public class ExampleArithmeticOperators {


    /*
     * Sum of the both a and b
     * */
    public int sum(int a, int b) {
        return a + b;
    }

    /*
     * return the difference between both a and b
     * */
    public int subtraction(int a, int b) {
        return a - b;
    }

    /*
     * return the multiplied value
     * */
    public int multiplication(int a, int b) {
        return a * b;
    }

    /*
     * returns the quo
     * */
    public int division(int a, int b) {
        return a / b;
    }

    /*
     * returns the remainder
     * */
    // This is a comment and never been read by java compiler
    public int modulus(int a, int b) {
        return a % b;
    }


    /*
     *
     * returns the value of a after incremented by 1
     * */
    public int increment(int a) {
        return a++;
    }

    /*
     *
     *      returns the value of a after decremented by 1
     * */
    public int decrement(int a) {
        return --a;
    }
}
