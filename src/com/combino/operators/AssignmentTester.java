package com.combino.operators;

public class AssignmentTester {
    public static void main(String[] args) {
        ExampleAssignmentOperators exampleAssignmentOperators = new ExampleAssignmentOperators();
        exampleAssignmentOperators.printAssignments();
        exampleAssignmentOperators.exampleTerenaryOperator();
    }
}
