package com.combino.operators;

public class Tester {
    public static void main(String[] args) {

        int p = 50, q = 10;

        ExampleArithmeticOperators arithmeticOperators = new ExampleArithmeticOperators();
        System.out.println("The sum of " + p + " and " + q + " is " + arithmeticOperators.sum(p, q));
        System.out.println("The difference of " + p + " and " + q + " is " + arithmeticOperators.subtraction(p, q));
        System.out.println("The multiplication of " + p + " and " + q + " is " + arithmeticOperators.multiplication(p, q));
        System.out.println("The division of " + p + " and " + q + " is " + arithmeticOperators.division(p, q));
        System.out.println("The modulus of " + p + " and " + q + " is " + arithmeticOperators.modulus(p, q));
        System.out.println("The increment of " + p + " is " + arithmeticOperators.increment(p));
        System.out.println("The decrement of " + p + " is " + arithmeticOperators.decrement(p));

        System.out.println("_______________Relational operators_________________");
        ExampleRelationalOperators relationalOperators = new ExampleRelationalOperators();
        System.out.println("equals between " + p + " and " + q + " is " + relationalOperators.equals(p, q));
        System.out.println("not equals between " + p + " and " + q + " is " + relationalOperators.notEquals(p, q));
        System.out.println("greater than between " + p + " and " + q + " is " + relationalOperators.greaterThan(p, q));
        System.out.println("less than between " + p + " and " + q + " is " + relationalOperators.lessThan(p, q));
        System.out.println("greater than or equals between " + p + " and " + q + " is " + relationalOperators.greaterThanOrEquals(p, q));
        System.out.println("less than or equals between " + p + " and " + q + " is " + relationalOperators.lessThanOrEquals(p, q));
        System.out.println("_______________Binary operators_________________");
        ExampleBinaryOperators binaryOperators = new ExampleBinaryOperators();
        binaryOperators.binaryAnd();
        binaryOperators.binaryOr();
        binaryOperators.binaryXor();
        binaryOperators.binaryCompliment();
        binaryOperators.binaryLeftShift();
        binaryOperators.binaryRightShift();

        boolean s = true, t= false;
        ExampleLogicalOperator logicalOperator = new ExampleLogicalOperator();
        System.out.println( logicalOperator.logicalAnd(s,t));
        System.out.println( logicalOperator.logicalOr(s,t));
        System.out.println( logicalOperator.logicalNot(s));


    }
}
