package com.combino.oops.inheritance;

public class Tester {
    public static void main(String[] args) {
        Child child = new Child();
        child.thisIsAMethodFromParentClass();
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        child.oneMoreMethodFromChild();
        System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%");

        Parent parent = new Child();
        parent.thisIsAMethodFromParentClass();
    }
}
