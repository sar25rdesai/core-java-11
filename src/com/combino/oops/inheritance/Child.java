package com.combino.oops.inheritance;

public class Child extends Parent {


    public Child(int id) {
        super(id);
    }

    public Child() {
    }

    @Override
    public void thisIsAMethodFromParentClass() {
        System.out.println("This is a method from the child class");
    }


    public void oneMoreMethodFromChild(){
       super.thisIsAMethodFromParentClass();
    }
}
