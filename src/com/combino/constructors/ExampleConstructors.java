package com.combino.constructors;

public class ExampleConstructors {

    private int x, y;

    public ExampleConstructors() {
        someMethod();
        System.out.println("The constructor of ExampleConstructors class has been invoked");

    }


    public ExampleConstructors(int x, int y) {
        this.x = x;
        this.y = y;
        System.out.println("You invoked parameterized constructor with variable values " + x + ", " + y);
    }

    public ExampleConstructors(int x) {
        this.x = x;
        System.out.println("You invoked parameterized constructor with one variable value " + x + ", " + y);

    }

    public void someMethod() {
        System.out.println("Some method has been invoked");
    }

}
